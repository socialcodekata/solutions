n input().split(' ')]
rows = []
for i in range(N):
    rows.append(input())

grid = [[column for column in row] for row in rows]
actions = ['U', 'D', 'L', 'R']

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.__update_action()
    
    def __update_action(self):
        if self.is_valid():
            self.cur_action = grid[self.y - 1][self.x - 1]
    
    def down(self):
        self.x, self.y = self.x, self.y + 1
        self.__update_action()
        
    def up(self):
        self.x, self.y = self.x, self.y - 1
        self.__update_action()
        
    def right(self):
        self.x, self.y = self.x + 1, self.y
        self.__update_action()
        
    def left(self):
        self.x, self.y = self.x - 1, self.y
        self.__update_action()

    def is_valid(self):
        if self.x < 1 or self.x > M or self.y < 1 or self.y > N:
            return False
        else:
            return True
        
    def get_key(self):
        return str(self.x) + '_' + str(self.y)
        
    def get_cur_action(self):
        return self.cur_action

def run_step(cur_pt, cur_action):
    next_pt = Point(cur_pt.x, cur_pt.y)
    if cur_action == "U":
        next_pt.up()
    elif cur_action == "D":
        next_pt.down()
    elif cur_action == "L":
        next_pt.left()
    elif cur_action == "R":
        next_pt.right()
    
    return next_pt

def find_coin(cur_pt, k, memory):
    if cur_pt.get_cur_action() == '*':
        return 0
    elif cur_pt.get_key() + "_" + str(k) in memory:
        return memory[cur_pt.get_key() + "_" + str(k)]
    elif k == 0 or not cur_pt.is_valid():
        return -1
    
    next_pt = run_step(cur_pt, cur_pt.get_cur_action())
    min_result = find_coin(next_pt, k - 1, memory)
    desired_action = cur_pt.get_cur_action()
    for action in filter(lambda action: action != desired_action, actions):
        next_pt = run_step(cur_pt, action)
        other_result = find_coin(next_pt, k - 1, memory)
        if other_result >= 0:
            result = 1 + other_result
            if result < min_result or min_result == -1:
                min_result = result
    
    memory[cur_pt.get_key() + "_" + str(k)] = min_result
    return min_result

start_pt = Point(1,1)
result = find_coin(start_pt, K, {})
print(result)
            

