T = int(input())

for i in range(T):
    M = int(input())
    N = int(input())
    cost = [int(x) for x in input().split()]
    
    histo = {}
    for idx, n in enumerate(cost):
        cream = histo.get(n, -1)
        if cream == -1:
            histo[n] = [idx + 1]
        else:
            histo[n].append(idx + 1)
    
    for idx, n in enumerate(cost):
        cream = M - n
        if cream in histo:
            if cream == n and len(histo[cream]) >= 2:
                print(histo[cream][0], histo[cream][1])
                break
            elif cream == n and len(histo[cream]) == 1:
                continue
            else:
                print(idx + 1, histo[cream][0])
                break
        
