T = int(input())

def gcd(a, b):
    if b > a:
        b, a = a, b

    while b != 0:
        c = b
        b = a % b
        a = c
            
    return a

def solution(l, b):
    m = gcd(l, b)
    #print(m)
    return int((l * b) / (m * m))

for t in range(T):
    l, b = [int(x) for x in input().split(' ')]
    print(solution(l, b))
    
