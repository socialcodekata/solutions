N = int(input())
m = []
for n in range(N):
    m.append([int(x) for x in input()])
    
def output(m):
    for r in m:
        print("".join([str(r_s) for r_s in r]))
    
def solution(m):
    n = len(m)
    #print(n)
    if n < 3:
        output(m)
        return
    
    cavity = []
    for r in range(1,n-1):
        for c in range(1,n-1):
            val = m[r][c]
            if m[r-1][c] < val and m[r][c-1] < val \
                and m[r+1][c] < val and m[r][c+1] < val:
                cavity.append([r,c])
    for cav in cavity:
        m[cav[0]][cav[1]] = 'X'
    output(m)
    
solution(m)
