def solution(sticks):
    if len(sticks) == 0:
        return
    
    sorted_sticks = sorted(sticks)
    min_seen = sorted_sticks[0]
    sticks_left = len(sorted_sticks)
    
    traveled_index = 0
    for i in range(len(sorted_sticks)):
        if sorted_sticks[i] > min_seen:
            print(sticks_left)
            sticks_left -= traveled_index
            min_seen = sorted_sticks[i]
            traveled_index = 0
        traveled_index += 1
    
    if min_seen != sorted_sticks[:-1]:
        print(sticks_left)

N = int(input())
sticks = [int(x) for x in input().split(' ')]
solution(sticks)
