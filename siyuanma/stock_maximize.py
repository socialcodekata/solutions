T = int(input())

def solution(days, prices):
    prices = list(reversed(prices))
    #print(prices)
    global_max = 0
    final_profit = 0
    bought_shares = []
    for price in prices:
        if price >= global_max:
            # sell all bought shares at the previous maximum
            for share_price in bought_shares:
                final_profit += global_max - share_price
            bought_shares = []
            global_max = price
        elif price < global_max:
            bought_shares.append(price)
    if len(bought_shares) != 0:
        for share_price in bought_shares:
            final_profit += global_max - share_price
    print(final_profit)
                
for t in range(T):
    days = int(input())
    prices = [int(x) for x in input().split(' ')]
    solution(days, prices)
