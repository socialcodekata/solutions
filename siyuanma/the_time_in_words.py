H = int(input())
M = int(input())

convertor = {
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five",
    6: "six",
    7: "seven",
    8: "eight",
    9: "nine",
    10: 'ten',
    11: 'eleven',
    12: 'twelve',
    13: 'thirteen',
    14: 'fourteen',
    15: 'quarter',
    16: 'sixteen', 
    17: 'seventeen',
    18: 'eighteen',
    19: 'nineteen',
    20: 'twenty',
    30: 'half'
}

class StaterMater:
    def __init__(self, H, M):
        self.template = "{} {}"
        
        if M > 30:
            next_hour = (H + 1) % 12
            if next_hour == 0:
                next_hour = 1
            self.suffix = "to " + convertor[next_hour]
        elif M == 0:
            self.prefix = convertor[H]
            self.suffix = "o' clock"
        else:
            self.suffix = "past " + convertor[H]
        
        self.__get_prefix(M)
        
    def __get_prefix(self, M):
        if M == 0:
            return
        
        if M > 30:
            M = 60 - M
            
        if M > 20 and M < 30:
            left_over = M - 20
            self.prefix = convertor[20] + " " + convertor[left_over]
        else:
            self.prefix = convertor[M]
            
        if M not in [15, 30, 1]:
            self.prefix += " minutes"
        elif M == 1:
            self.prefix += " minute"
            
    def render(self):
        print(self.template.format(self.prefix, self.suffix))
        
sm = StaterMater(H, M)
sm.render()
