def solution(array):
    return ""

def parse_input():
    T = int(input())
    for t in xrange(T):
        n = int(input())
        array = [int(x) for x in input().split(' ')]
        print(solution(array))

import unittest
class TestCases(unittest.TestCase):

    def test_basic(self):
        self.assertEqual(solution([1,2,3,4]), "10 10")
        self.assertEqual(solution([2,-1,2,3,4,-5]), "10 11")

if __name__ == "__main__":
    unittest.main()
