N = int(input())
steps = [int(input()) for n in range(N)]

cost = 0
for s in steps:
    cost += s * 0.5
    
print(cost)
