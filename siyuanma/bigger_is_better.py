N = int(input())

def solution(chars):
    reversed_chars = list(reversed(chars))
    ans = []
    fix_up_point = -1
    seen = []

    for i, ch in enumerate(reversed_chars):
        if len(seen) > 0:
            # check if any characters we have seen is larger than the current
            for seen_ch in seen:
                if seen_ch > ch:
                    # if we are not at the end of the array
                    if i < len(reversed_chars) - 1:
                        # reversed the values that are unchanged in the reversed array and append it to our ans
                        ans = list(reversed(reversed_chars[i + 1:len(reversed_chars)]))
                    # track the index of the character we want to swap
                    fix_up_point = i
                    break
        seen.append(ch)
        if fix_up_point != -1:
            break
    
    if (fix_up_point == -1):
        return "no answer"
   
    # get the character we want to swap out of the reversed array 
    fix_up_char = reversed_chars[fix_up_point]
    best_candidate = None
    # locate the minimum character that is larger then the fix_up_char
    for seen_char in seen:
        if seen_char > fix_up_char:
            if not best_candidate or seen_char < best_candidate:
                best_candidate = seen_char

    ans.append(best_candidate)
    seen.remove(best_candidate)
    
    # proceed to append the min characters until completion    
    for i in range(len(seen)):
        min_char = min(seen)
        seen.remove(min_char)
        ans.append(min_char)
        
    return ''.join(ans)

for n in range(N):
    characters = str(input())
    print(solution(characters))
