import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int n1 = in.nextInt();
        int[] array1 = new int[n1];
        for(int i = 0; i < n1; i++) {
            array1[i] = in.nextInt();           
        }
        int n2 = in.nextInt();
        int[] array2 = new int[n2];
        for(int i = 0; i < n2; i++) {
            array2[i] = in.nextInt();           
        }
        MissingNumber mn = new MissingNumber(array1, array2);
        System.out.println(mn.printMissing());
    }
}

class MissingNumber{
    private Map<Integer, Integer> map;
    private int[] listToCompare;
    
    public MissingNumber(int[] list1, int[] list2) {
        if (list1.length < list2.length) {
            map = createMapFromList(list2);
            listToCompare = list1;
        } else {
            map = createMapFromList(list1);
            listToCompare = list1;
        }
    }
    
    public String printMissing () {
        Set<Integer> set = getMissing();
        List<Integer> list = new ArrayList(set);
        Collections.sort(list);
        StringBuffer sb = new StringBuffer();
        sb.append(list.get(0));
        for (int i = 1; i < list.size(); i++) {
            sb.append(" ").append(list.get(i));
        }
        return sb.toString();
    }

    public Set<Integer> getMissing() {
        for (int i : listToCompare) {
            if (map.containsKey(i)) {
                int freq = map.get(i);
                if (freq == -1){
                    continue;
                } else {
                    freq--;
                    if (freq == 0) {
                        map.remove(i);
                    } else {
                        map.put(i, freq);
                    }
                }
            } else {
                //not in the map, then mark -1
                map.put(i, -1);
            }
        }
        return map.keySet();
    }
    
    private Map<Integer, Integer> createMapFromList(int[] list) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i : list) {
            if (map.containsKey(i)) {
                int freq = map.get(i);
                map.put(i, ++freq);
            } else {
                map.put(i, 1);
            }
        }
        return map;
    }
}
