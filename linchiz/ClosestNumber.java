import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] array = new int[n];
        for(int i = 0; i < n; i++) {          
            array[i] = in.nextInt();
        }
        System.out.println(new ClosestNumber(array).printClosestPairs());     
    }
}

class ClosestNumber {
    private int[] array;
    
    public ClosestNumber (int[] array) {
        this.array = array;
    }
    
    public String printClosestPairs() {
        if (array.length < 2){
            return "";
        }
        List<Integer> pairs = getClosestPairs();
        StringBuffer sb = new StringBuffer();
        for (int i : pairs) {
            sb.append(i).append(" ");
        }
        return sb.toString();
    }
    
    private List<Integer> getClosestPairs() {
        Arrays.sort(array);
        List<Integer> pairs = new ArrayList<Integer>();
        int diff = Integer.MAX_VALUE;
        for (int i = 1 ; i < array.length; i++) {
            if (array[i] - array[i-1] < diff) {
                pairs.clear();
                diff = array[i] - array[i-1];
                pairs.add(array[i-1]);
                pairs.add(array[i]);
            } else if (array[i] - array[i-1] == diff) {
                pairs.add(array[i-1]);
                pairs.add(array[i]);
            }
        }
        return pairs;
    }
}
