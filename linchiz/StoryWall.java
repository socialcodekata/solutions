import java.util.ArrayList;
import java.util.Date;
import java.util.List;


class Project {
	private List<Iteration> iterations;
	private int totalDonePoints=0;
	private int totalPoints=0;
	public Project() {
		iterations = new ArrayList<Iteration>();
	}
	public void addIteration(Iteration iter) {
		iterations.add(iter);
		iter.addToProject(this);
		updateTotalPoints(iter.getTotalPoints());
		updateDonePoints(iter.getTotalDonePoints());
	}

	public void updateDonePoints(int points) {
		totalDonePoints += points;
	}
	public void updateTotalPoints(int points) {
		totalPoints += points;
	}
	public int getTotalPoints() {
		return totalPoints;
	}
	public int getTotalDonePoints() {
		return totalDonePoints;
	}
}

class Iteration {
	private List<Story> stories;
	private int totalPoints=0;
	private int totalDonePoints=0;
	private Project addToProject;
	private boolean isClosed;
	public Iteration(){
		stories = new ArrayList<Story>();
		totalPoints = 0;
		isClosed = false;
	}
	public void add(Story oneStory) throws Exception {
		if (!isClosed) {
			stories.add(oneStory);
			oneStory.addToIteration(this);
			updateTotalPoints(oneStory.getPoints());
			updateDonePoints(oneStory.getDonePoints());
		} else {
			throw new Exception("this iteration is closed");
		}
	}
	public void closeIteration() {
		isClosed = true;
	}
	public int getTotalPoints() {
		return totalPoints;
	}
	public int getTotalDonePoints() {
		return totalDonePoints;
	}
	public void updateTotalPoints(int points) {
		totalDonePoints += points;
		if (addToProject != null) {
			addToProject.updateDonePoints(points);
		}
	}
	public void updateDonePoints(int points) {
		totalDonePoints += points;
		if (addToProject != null) {
			addToProject.updateDonePoints(points);
		}
	}
	public void addToProject(Project project) {
		addToProject = project;
	}
	
}

class Story {
	enum SwimLane {
		ReadyForDev(0), InDev(1), CodeReview(2), QA(3), Done(4);
		private final int value;
		private SwimLane (final int value) {
			this.value = value;
		}
		public int getValue() {
			return this.value;
		}

	}
	class SwimLaneEntry{
		public Date enter;
		public Date exit;
		public SwimLane status;
		public SwimLaneEntry(SwimLane status) {
			this.status = status;
		}
	}
	private Iteration addToIteration;
	private SwimLane progress;
	private String title;
	private int points=0;
	private int donePoints = 0;
	private List<SwimLaneEntry> records;
	public Story(String title, int points) {
		this.title = title;
		this.points = points;
		records = new ArrayList<SwimLaneEntry>();
	}

	public void addToIteration(Iteration iter) {
		addToIteration = iter;
	}
	public void moveToSwimlane(SwimLane lane) throws Exception {
		if (this.progress != null) {
			int dist = this.progress.getValue() - lane.getValue();
			if (dist == 0) {
				return;
			}
			if (dist > 1 || dist < -1 ) {
				throw new Exception("cannot skip lane");
			}
		}
		recordEntry(lane);
		if (lane == SwimLane.Done) {
			donePoints = this.points;
			addToIteration.updateDonePoints(this.points);
		}
	}
	private void recordEntry(SwimLane status) {
		SwimLaneEntry newRecord = new SwimLaneEntry(status);
		Date timenow = new Date();
		records.get(records.size()-1).exit = timenow;
		newRecord.enter = timenow;
		records.add(newRecord);
	}

	public int getPoints(){
		return points;
	}
	public int getDonePoints(){
		return donePoints;
	}	
	public String getTitle() {
		return title;
	}
	public List<SwimLaneEntry> getProgressRecords() {
		return records;
	}
}


