import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    	public static final int END=100;

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);

        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            Map<Integer,Integer> ladders = new HashMap<Integer,Integer>();
            Map<Integer,Integer> snakes = new HashMap<Integer,Integer>();
            int n = in.nextInt();
            for (int j = 0; j < n; j++) {
                ladders.put(in.nextInt(), in.nextInt());
            }

            int m = in.nextInt();
            for (int j = 0; j < m; j++) {
                snakes.put(in.nextInt(), in.nextInt());
            }
            System.out.println(solve(ladders, snakes));
        }    
    }
     private static int solve (Map<Integer, Integer> ladders, Map<Integer, Integer> snakes) {
        Queue<Integer> nodes = new LinkedList<Integer>();
        Set<Integer> added = new HashSet<Integer>();
        int currentLevelNodes = 1;
        int nextLevelNodes = 0;
        int steps = 0;
        nodes.offer(1);
        added.add(1);
        while (!nodes.isEmpty()) {
        	int current = nodes.poll();
			if (current == END) {
    			return steps;
    		}
        	currentLevelNodes--;
        	for (int i = 1; i <= 6; i++) {
        		int next = current + i;
        		if (ladders.containsKey(next)) {
        			next = ladders.get(next);
        			if (!added.contains(next)) {
        				nodes.offer(next);
        				added.add(next);
                		nextLevelNodes++;       				
        			}
        		} else if (snakes.containsKey(next)) {
        			next = snakes.get(next);
        			if (!added.contains(next)) {
        				nodes.offer(next);
        				added.add(next);
                		nextLevelNodes++;       				
        			}
        		} else {

        			if (!added.contains(next)) {
        				nodes.offer(next);
        				added.add(next);
                		nextLevelNodes++;  
        			}
        		}

        	}
        	if (currentLevelNodes == 0) {
        		currentLevelNodes = nextLevelNodes;
        		nextLevelNodes = 0;
        		steps++;
        	}
        }
        return -1;
    }
    
}
