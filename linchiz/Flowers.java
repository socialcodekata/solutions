/* Sample program illustrating input/output methods */
import java.util.*;

class Solution{
   public static void main( String args[] ){
      
// helpers for input/output      

      Scanner in = new Scanner(System.in);
      int n = in.nextInt();
      int k = in.nextInt();
      
      int c[] = new int[n];
      for(int i=0; i<n; i++){
         c[i] = in.nextInt();
      }
      
      System.out.println( new Flower(c, k).getMinPrice());
      
   }
}

class Flower{
    private int[] prices;
    private int buyers;
    
    public Flower(final int[] c, final int k) {
        prices = c;
        buyers = k;
    }
    
    public int getMinPrice() {
        Arrays.sort(prices);
        int total = 0;
        int round = 0;
        int buyer = 0;
        for (int i = prices.length-1; i >=0;  i--) {
            total += prices[i] * (round + 1);
            if (++buyer == buyers) {
                round++;
                buyer = 0;
            }
        }
        return total;
    }
}
