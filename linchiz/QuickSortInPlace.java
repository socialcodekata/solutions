import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] array =  new int[n];
        for(int i = 0; i < n; i++) {
            array[i] = in.nextInt();
        }
        System.out.println(new QuickSortInPlace(array).printSort());
    }
}
    
class QuickSortInPlace{
    private int[] array;
    private StringBuffer buffer;
    
    public QuickSortInPlace(int[] array) {
        this.array = array;
    }
    public String printSort() {
        buffer = new StringBuffer();
        sortForPivot(0, array.length-1);
        return new String(buffer);
    }
    
    private String printArray() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i<array.length; i++) {
            sb.append(array[i]).append(" ");
        }
        return new String(sb);
    }
    private void sortForPivot(int start, int end) {
        int availableInd = start-1;
        for (int compareWithInd = start; compareWithInd <= end; compareWithInd++) {
            if (array[compareWithInd] <= array[end]) {
               swap(++availableInd,compareWithInd);
            } 
        }
        buffer.append(printArray()).append("\n");
        if (start < availableInd-1) {
            sortForPivot(start, availableInd-1);  
        }
        if (availableInd+1 < end) {
            sortForPivot(availableInd+1, end);   
        }
        return;
    }
    private void swap(int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }   
}
