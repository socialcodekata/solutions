import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int tests = in.nextInt();        
        for(int i = 0; i < tests; i++) {
            int m = in.nextInt();
            int n = in.nextInt();
            int[] y = new int[m-1];
            int[] x = new int[n-1];
            for (int yi = 0; yi < m-1; yi++){
                y[yi] = in.nextInt();
            }
            for (int xi = 0; xi < n-1; xi++){
                x[xi] = in.nextInt();
            }
            System.out.println(new BoardCutting(y,x).minCost());
        }
        
    }
}

class BoardCutting {
    private int[] y;
    private int[] x;
    private final long MOD = 1000000007;
    public BoardCutting(int[] y, int[] x) {
        this.y = y;
        this.x = x;     
    }
    
    public long minCost() {
        Arrays.sort(x);
        Arrays.sort(y);
        long cost = 0;
        final int m = y.length;
        final int n = x.length;
        int i = 1;
        int j = 1;
        while (i <= m && j <= n){
            if (y[m-i] > x[n-j]){
                //needs to cast long first, the multiplication can overflow int
                cost += (long)y[m-i] * j;
                i++;
            } else {
                cost += (long)x[n-j] * i;
                j++;
            }
        }
        while (i <= m) {
            cost += (long)y[m-i] * j;
            i++;
        }
        while (j <= n) {
            cost += (long)x[n-j] * i;
            j++;
        }
        return cost%MOD;
    }
}
