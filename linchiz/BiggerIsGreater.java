import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
       Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for(int i = 0; i < n; i++) {
            final String input = in.next();
            BiggerIsGreater bg = new BiggerIsGreater(input);
            System.out.println(bg.getNextBigger());
        }
    }
}

 class BiggerIsGreater{
    private char[] charArray;
    private int size;
    
    public BiggerIsGreater(String input) {
        this.size = input.length();
        this.charArray = input.toCharArray();
//        assertTrue(size>1);
    }
    
    public String getNextBigger() {
        int posToSwap = getPosToSwap();
        if (posToSwap == -1) return "no answer";
        int posToSwapWith = getPosToSwapWith(posToSwap);
        swapChar(posToSwap, posToSwapWith);
        reverseTail(posToSwap+1);
        return new String(charArray);
    }    
    private int getPosToSwap() {
        for(int i = size-1; i > 0; i--) {
            if (charArray[i] > charArray[i-1]){
                return i-1;
            }
        }
        return -1;
    }
    private int getPosToSwapWith(int pos) {
        for(int i = size-1; i > pos; i--) {
            if (charArray[i] > charArray[pos]){
                return i;
            }
        }
        //this should never happen as charArray[pos+1] > charArray[pos] 
        return -1;
    }
    private void swapChar(int i, int j) {
        char temp = charArray[i];
        charArray[i] = charArray[j];
        charArray[j] = temp;
    }
    private void reverseTail(int pos) {
        int i = pos;
        int j = size-1;
        while(j > i) {
            swapChar(i++, j--);
        }
    }
}
