import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int testCases = in.nextInt();
        for (int i = 0; i < testCases; i++) {
            int length = in.nextInt();
            int breadth = in.nextInt();
            System.out.println(cut(length, breadth));
        }
        
        return;
    }
    
    public static int cut(int length, int breadth){
        int n = 1;
        if(length < breadth){
            n = gcd(length, breadth);
        }
        else{
            n = gcd(breadth, length);
        }
        return length*breadth/n/n;
    }

    public static int gcd(int a, int b) {
        for(int i = a; i > 0; i--) {
            if(a%i==0 && b%i==0){
                return i;
            }
        }
        return 1;
    }
}
