import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        
        for (int i = 0; i < n; i++){
            char[][] grid = createBoard (in);
            char[][] pattern = createBoard (in);
            GridSearch gs = new GridSearch(grid, pattern);
            System.out.println( gs.hasPattern()? "YES":"NO");
        }
        
    }    
    private static char[][] createBoard(Scanner in) {
        int row = in.nextInt();
        int column = in.nextInt();
        char[][] board = new char[row][column];
        for (int r = 0; r < row; r++) {
            char[] line = in.next().toCharArray();
            board[r] = line;
        }
        return board;
    }
}

class GridSearch {
    private char[][] grid;
    private char[][] pattern;
    private int gr;
    private int gc;
    private int pr;
    private int pc;
    
    public GridSearch (char[][] grid, char[][] pattern) {
        this.grid = grid;
        this.pattern = pattern;
        this.gr = grid.length;
        this.gc = grid[0].length;
        this.pr = pattern.length;
        this.pc = pattern[0].length;
        
    }
    
    public boolean hasPattern() {
        for (int i = 0; i <= gc-pc; i++) {
            for (int j = 0; j <= gr-pr; j++) {
                if (grid[i][j] == pattern[0][0]) {
                    if (searchPatternFrom(i, j)){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private boolean searchPatternFrom (int row, int col) {
        for (int i = 0; i < pr; i++) {
            for (int j = 0; j < pc; j++ ) {
                if (pattern[i][j] != grid[row+i][col+j]){
                    return false;
                }
            }
        }
        return true;
    }
}
