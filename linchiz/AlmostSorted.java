import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
 
public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] array = new int[n];
        for(int i = 0; i < n; i++) {
            array[i] = in.nextInt();           
        }
        AlmostSorted as = new AlmostSorted(array);
        System.out.println(as.oneOpSort());
    }
}
class AlmostSorted{
    private int[] input;
    private final int n;
    private String YES = "yes";
    private String NO = "no";

    public AlmostSorted(int[] input) {
        this.input = input;
        n = input.length;
    }
    public String oneOpSort() {
        int firstMax = increaseTill(0);
        if (firstMax == n-1) {
            return YES;
        } else {
            int firstMin = decreaseTill(firstMax);
            if (firstMin == n-1) {
                if (firstMin == firstMax+1) {
                    return opsStr(firstMax, firstMin, "swap");
                }
                if (firstMax==0 || input[firstMax-1] <= input[firstMin]){
                    return opsStr(firstMax, firstMin, "reverse");
                }
            } else if (firstMin == firstMax + 1) {
                int secMax =  increaseTill(firstMin);
                if (secMax == n-1) return NO;
                else {
                    if (increaseTill(secMax+1)!= n-1 || input[secMax+1] > input[firstMin]
                        || (firstMax > 0 && input[secMax+1] < input[firstMax-1])
                        || input[firstMax] < input[secMax]
                        || (secMax < n-2 && input[firstMax] > input[secMax+2])){
                        return NO;
                    }
                    else {
                        return opsStr(firstMax, secMax+1, "swap");
                    }
                }
            }else {
                //possible reverse
                if (increaseTill(firstMin) == n-1
                    && input[firstMin] >= input[firstMax-1] && input[firstMin+1] >= input[firstMax]) {
                    return opsStr(firstMax, firstMin, "reverse");
                }
                else {
                    return NO;
                }
            }
        }
        return NO;
    }

    private int increaseTill(int from) {
        if (from==n-1) return n-1;
        for (int i = from; i < n-1; i++) {
            if (input[i+1] < input [i]){
                return i;
            }
        }
        return n-1;
    }
    private int decreaseTill(int from) {
        if (from==n-1) return n-1;
        for (int i = from; i < n-1; i++) {
            if (input[i+1] > input [i]){
                return i;
            }
        }
        return n-1;
    }
    private String opsStr (int i, int j, String op) {
        return YES + "\n" + op + " " + (i+1) + " " + (j+1);
    }    
   
}
