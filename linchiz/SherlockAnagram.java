import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for(int i = 0; i < n; i++) {
            final String input = in.next();
            SherlockAnagram sa = new SherlockAnagram(input);
            System.out.println(sa.countAnagramPairs());
        }
    }
}

class SherlockAnagram {
    private char[] charArray;
    private int n;
    private Map<String, Integer> anagramCount;
    
    public SherlockAnagram (String input) {
        n = input.length();
        charArray = input.toCharArray();
        anagramCount = new HashMap<String, Integer>();
    }
    
    public int countAnagramPairs() {
        int pairs = 0;
        for (int subLength = 1; subLength < n; subLength++) {
            pairs += countAnagramOfLength(subLength);
        }
        return pairs;
    }
    
    private int countAnagramOfLength(int length) {
        int count = 0;
        for (int start = 0; start + length <= n; start++) {
            final String sorted = getSortedString(start, length);
            if (anagramCount.containsKey(sorted)) {
                int anagramFound = anagramCount.get(sorted);
                // this substring can make a pair with every anagram found by same key
                count += anagramFound;
                anagramCount.put(sorted, ++anagramFound);
            } else {
                anagramCount.put(sorted, 1);
            }
        }
        return count;
    }

    private String getSortedString(int startIndex , int length) {
        char[] newArray = Arrays.copyOfRange(charArray, startIndex, startIndex+length); 
        Arrays.sort(newArray);
        return new String(newArray);
    }
}
