import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    private static final String[] SAY_NUMBER={
"zero","one", "two", "three", "four", "five", "six", "seven","eight","nine","ten",
"eleven", "twelve","thirteen","forteen","fivteen","sixteen","seventeen","eighteen","nineteen","twenty"};

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int hour = in.nextInt();
        int minute = in.nextInt();
        if(minute == 0){
            System.out.println(sayHour(hour)+ " o' clock");           
        }
        else if(minute > 30){
            System.out.println(sayMinute(60-minute) + " to "+sayHour(hour+1));
        }
        else{
            System.out.println(sayMinute(minute) + " past "+sayHour(hour));
        }
    }
    
    private static String sayMinute(int minute) {
        if (minute == 1){
            return "one minute";
        }else if(minute == 15){
            return "quarter";
        }
        else if(minute == 30){
            return "half";
        }
        else {
            if(minute < 20){
                return SAY_NUMBER[minute]+" minutes";
            }
            else {
                return SAY_NUMBER[20] + " " +SAY_NUMBER[minute-20]+" minutes";
            }
        }
    }
    
    private static String sayHour(int hour) {
        return SAY_NUMBER[hour];
    }
}
