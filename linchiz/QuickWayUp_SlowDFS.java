
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SnakeAndLadder {
	public static final int END=20; //100 is too big for this slow algrithm
    public static void main(String[] args) {
        System.out.println("enter input");
    	Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            Map<Integer,Integer> ladders = new HashMap<Integer,Integer>();
            Map<Integer,Integer> snakes = new HashMap<Integer,Integer>();
            int n = in.nextInt();
            for (int j = 0; j < n; j++) {
                ladders.put(in.nextInt(), in.nextInt());
            }

            int m = in.nextInt();
            for (int j = 0; j < m; j++) {
                snakes.put(in.nextInt(), in.nextInt());
            }
            System.out.println("output");
            System.out.println(solve(ladders, snakes));
        } 
        in.close();
    }
    
    private static int solve (Map<Integer, Integer> ladders, Map<Integer, Integer> snakes) {
        int[] visited = new int[END+1];
        return stepsFrom (1, 0, ladders, snakes, visited);
    }
    
    private static int stepsFrom (int start, int step, Map<Integer, Integer> ladders, Map<Integer, Integer> snakes, int[] visited) {
        visited[start] = 1;
        if (start == END) {
        	System.out.println("arrived in " + step);
            visited[start] = 0;
            return step;
        }
        if (ladders.containsKey(start)) {
            int nextStart = ladders.get(start);
            if (visited[nextStart] == 0) {
            	System.out.println("ladders to " + nextStart);
                int s = stepsFrom(nextStart, step, ladders, snakes, visited);
                visited[start] = 0;
                return s;
            } else {
                visited[start] = 0;
                return END;
            }
        }
        if (snakes.containsKey(start)) {
            int nextStart = snakes.get(start);
            if (visited[nextStart] == 0) {
            	System.out.println("snakes to " + nextStart);
                int s = stepsFrom(nextStart, step, ladders, snakes, visited);
                visited[start] = 0;
                return s;
            } else {
                visited[start] = 0;
                return END;
            }
            
        }
        
        int minsteps = END;
        for (int i = 5; i >=0; i--) {
            if (start+1+i <= END &&  visited[start+1+i] == 0) {
            	System.out.println("move to " + (start+1+i));
                int stepi= stepsFrom(start+i+1, step+1, ladders, snakes, visited);
            	System.out.println("use step " + stepi);
                minsteps = minsteps > stepi ? stepi : minsteps;
            }
        }
        visited[start] = 0;
        return minsteps;
        
    }
}
