import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int testCases = in.nextInt();
        for (int i = 0; i < testCases; i++) {            
            int n = in.nextInt();
            int[] tests = new int[n];
            for (int k = 0; k < n; k++) {
                tests[k] = in.nextInt();
            }           
            System.out.println(getProfit(tests));
        }
        
        return;
    }
    
    private static long getProfit(int[] prices) {
        int n = prices.length;
        int max = prices[n-1];
        
        long profit = 0;
        for( int i = n-1; i >=0; i--) {
           if (prices[i] <= max){
               profit += max-prices[i];
           }
           else {
                max = prices[i];
           }           
        }
        return profit;
    }
}
