import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for(int i = 0; i < n; i++) {
            int length = in.nextInt();
            int[] array = new int[length];
            for (int j = 0; j < length; j++) {
                array[j] = in.nextInt();
            }
            SherlockArray aa = new SherlockArray(array);
            System.out.println(aa.canBalance()?"YES":"NO");
        }
    }
}

class SherlockArray {
    private int[] array;
    private int n;
    private int[] sumForward;
    private int[] sumBackward;

    public SherlockArray (int[] input) {
       n = input.length;
       array = input;
       sumForward = new int[n];
       sumBackward = new int[n];
    }
    
    public boolean canBalance(){
        if (n==1) return true;
        addForward();
        addBackward();
        for (int i = 1; i < n-1; i++) {
            if (sumForward[i-1] == sumBackward[i+1]){
                return true;
            }
        }
        return false;
    }
    private void addForward(){
        sumForward[0] = array[0];
        for (int i = 1; i < n; i++) {
            sumForward[i] = sumForward[i-1] + array[i];
        }
    }
    private void addBackward(){
        sumBackward[n-1] = array[n-1];
        for (int i = n-2; i >=0; i--) {
            sumBackward[i] = sumBackward[i+1] + array[i];
        }
    }
    
}
