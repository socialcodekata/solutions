import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static int[] width;
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int length = in.nextInt();
        int t = in.nextInt();
        width = new int[length];
        for(int i = 0; i < length; i++) {
            width[i] = in.nextInt();
        }
        for(int i = 0; i< t; i++){
            System.out.println(getMaxWidth (in.nextInt(), in.nextInt()));
        }        
    }
    
    public static int getMaxWidth(int i, int j) {
        int max = 3;
        for(int k = i; k<=j; k++){
            if(width[k] < max){
                max = width[k];
                if(max==1)
                    return max;
            }
        }
        return max;
    }
}
