import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int numTests = sc.nextInt();
        for (int i = 0; i < numTests; i++) {
            String input = sc.next();
            int K = sc.nextInt();
            MyTuple[] suffixArray = generateSuffixArray(input, K);
            int[] lcp = generateLCPArray(input, suffixArray, K);
            System.out.println(solve(input, suffixArray, lcp, K));
        }
    }
    
    // copied from sample code
    private static MyTuple[] generateSuffixArray(String input, int K) {
        int N = input.length();
        int[][] suffixRank = new int[30][100000];
        for (int i =0; i < N; i++) {
            suffixRank[0][i] = input.charAt(i) - 'a';
        }
        MyTuple[] L = new MyTuple[N];
        
        for (int cnt = 1, stp = 1; cnt < N; cnt *=2, stp++ ) {
            for (int i =0; i < N; i++) {
                L[i] = new MyTuple();
                L[i].firstHalf = suffixRank[stp - 1][i];
                L[i].secondHalf = i + cnt < N ? suffixRank[stp - 1][i + cnt] : -1;
                L[i].originalIndex = i;
            }
            
            Arrays.sort(L);

            suffixRank[stp][L[0].originalIndex] = 0;
            
            for(int i = 1, currRank = 0; i < N; ++i) {
                
                if(L[i - 1].firstHalf != L[i].firstHalf || L[i - 1].secondHalf != L[i].secondHalf)
                    ++currRank;

                suffixRank[stp][L[i].originalIndex] = currRank;
            }
        }     
        return L;
    }
    
    // copied from sample code
    private static int[] generateLCPArray(String input, MyTuple[] sa, int K) {
        int n = input.length(), k =0;
        int[] lcp = new int[n];
        int[] rank = new int[n];
        for (int i = 0;i < n; i++) {
            lcp[i] = -1;
        }
        
        for (int i = 0;i<n;i++) {
            rank[sa[i].originalIndex] = i;
        }

        for (int i = 0; i < n; i++) {
            if(rank[i]==n-1) {k=0; continue;}
            int j=sa[rank[i]+1].originalIndex;
            while(i+k<n && j+k<n && input.charAt(i+k)==input.charAt(j+k)) k++;
            lcp[rank[i]]=k;
            if (k != 0) {
                k--;
            }
        }
        return lcp;
    }
    
    // solves the problem
    private static char solve(String input, MyTuple[] sa, int[] lcp, int K) {
        int N = input.length();
        
        // cumLength array represents cumulative length of each entry in suffix array
        long[] cumLength = new long[N];
        cumLength[0] = computeSeriesSum(N - sa[0].originalIndex);
        for (int i = 1; i < N; i++) {
            long num1 = computeSeriesSum(N - sa[i].originalIndex);
            long dupNum = computeSeriesSum(lcp[i-1]);
            cumLength[i] = num1 - dupNum + cumLength[i-1];
        }
        
        
        int index = searchArrayIndex(cumLength, K);
        // we have found the slot in the suffix array, now search for the substring
        int start = sa[index].originalIndex;
        int startNum = 1;
        if (index != 0) {
            startNum = lcp[index-1] + 1;
        }
        
        long[] finalCumLength = new long[N - start];
        finalCumLength[0] = startNum++;
        for (int i = 1; i < finalCumLength.length; i++) {
            finalCumLength[i] = startNum++ + finalCumLength[i-1];
        }
        
        long newK = K;
        if (index != 0) {
            newK = K - cumLength[index-1];
        }
        
        int index2 = searchArrayIndex(finalCumLength, (int)newK);   
        // we have found the substring
        
        long finalIndex = newK;
        if (index2 != 0) {
            finalIndex = newK - finalCumLength[index2-1];
        }
        return input.charAt(sa[index].originalIndex + (int)finalIndex - 1);
    }
    
    private static long computeSeriesSum(long n) {
        return n * (n+1) / 2;
    }
    
    private static int searchArrayIndex(long[] arr, int K) {
        int index = Arrays.binarySearch(arr, K);
        if (index < 0) {
            index = -index-1;
        }
        return index;
    }
    
    static class MyTuple implements Comparable<MyTuple> {
        int originalIndex;
        int firstHalf;
        int secondHalf;
        
        public int compareTo(MyTuple other) {
            if(this.firstHalf == other.firstHalf) {
                return this.secondHalf - other.secondHalf;
            }
            else return this.firstHalf - other.firstHalf;  
        }
    }
    
}
