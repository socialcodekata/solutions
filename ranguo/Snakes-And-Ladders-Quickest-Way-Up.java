import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import static java.util.Map.Entry;

public class Solution {

    
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        
        Scanner sc = new Scanner(System.in);
        int numTests = sc.nextInt();
        for (int i = 0; i < numTests; i++) {
            int numLadders = sc.nextInt();
            Map<Integer, Integer> ladders = new HashMap<>();
            for (int j = 0; j < numLadders; j++ ) {
                ladders.put(sc.nextInt(), sc.nextInt());
            }
            int numSnakes = sc.nextInt();
            Map<Integer, Integer> snakes = new HashMap<>();
            for (int j = 0; j < numSnakes; j++) {
                snakes.put(sc.nextInt(), sc.nextInt());
            }
            Map<Integer, Node> nodes = new HashMap<>();
            for (int j = 1; j <= 100; j++) {
                Node n = new Node();
                n.value = j;
                List<Integer> neighbors = new ArrayList<>();
                n.neighbors = neighbors;
                if (ladders.get(j) != null) {
                    neighbors.add(ladders.get(j));
                } else if (snakes.get(j) != null) {
                    neighbors.add(snakes.get(j));
                } else {
                    for (int k = 1; k <= 6; k++) {
                        neighbors.add(k+j);
                    }
                }
                nodes.put(j, n);
            }
            System.out.println(solve(1, nodes));
        }
    }
    
    public static class Node {
        public int distance = -1;
        public List<Integer> neighbors;
        public int value;
    }
    
    public static int solve(int start, Map<Integer, Node> nodes) {
        Queue<Node> q = new LinkedList<>();
        Node startNode = nodes.get(start);
        q.add(startNode);
        startNode.distance = 0;
        while (!q.isEmpty()) {
            Node node = q.poll();
            for (Integer neighborIndex : node.neighbors) {
                Node neighbor = nodes.get(neighborIndex);
                if (neighbor.distance != -1) continue;
                if (node.neighbors.size() == 1) {
                    neighbor.distance = node.distance;
                } else {
                    neighbor.distance = node.distance + 1;
                }
                
                if (neighbor.value == 100) {
                    return neighbor.distance;
                }
                q.add(neighbor);
            }
        }
        return -1;
    }
}
