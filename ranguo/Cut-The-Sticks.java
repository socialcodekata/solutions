import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int totalSticks = sc.nextInt();
        ArrayList<Integer> arr = new ArrayList<>();
        for (int i = 0; i < totalSticks; i++) {
            arr.add(sc.nextInt());
        }
        System.out.println(arr.size());
        Collections.sort(arr);
        while (true) {
            int first = arr.get(0);
            ArrayList<Integer> newArr = new ArrayList<>();
            for (int num : arr) {
                int newNum = num - first;
                if (newNum != 0) {
                    newArr.add(newNum);    
                }
            }
            if (newArr.size() == 0) break;
            System.out.println(newArr.size());
            arr = newArr;
        }
    }
}