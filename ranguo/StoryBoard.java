import java.util.*;

public class StoryBoard {
	static class Project {
		private List<Iteration> iterList = new ArrayList<>();
		void addIteration(Iteration iter) {
			iterList.add(iter);
		}	

		int getDonePoints() {
			int total = 0;
			for (Iteration iter : iterList) {
				total += iter.getDonePoints();
			}
                        return total;
		}

		int getTotalPoints() {
			int total = 0;
			for (Iteration iter : iterList) {
				total += iter.getTotalPoints();
			}
                        return total;
		}
	}

	static class Iteration {
		private List<Story> userList = new ArrayList<>();;
		private int totalPoints = 0;
		private boolean isClosed = false;

		void add(Story user) {
			if (isClosed) return;
			userList.add(user);
			totalPoints += user.getPoints();
		}

		int getTotalPoints() {
			return totalPoints;
		}

		void close() {
			isClosed = true;
		}

		int getDonePoints() {
			int total = 0;
			for (Story s : userList) {
				if (s.getSwimlane() == Swimlane.DONE) {
					total += s.getPoints();
				}
			}
                        return total;
		}
	}

	static class Story {
		private String description;
		private int points;
		private Swimlane swimlane;
		private Map<Long, Swimlane> historyMap = new TreeMap<>();

		Story(String description, int points) {
			this.description = description;
			this.points = points;
			historyMap.put(System.currentTimeMillis(), Swimlane.READY_FOR_DEV);
		}

		public int getPoints() {
			return points;
		}

                public Swimlane getSwimlane() {
                    return swimlane;
                }

		void moveToSwimLane(Swimlane swimlane) {
			this.swimlane = swimlane;
			historyMap.put(System.currentTimeMillis(), swimlane);
		}
	} 

	enum Swimlane {
		READY_FOR_DEV,IN_DEV,CODE_REVIEW,QA,DONE;
	}

	public static void main(String[] args) {
		
	}
}
