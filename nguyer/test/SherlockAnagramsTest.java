import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class SherlockAnagramsTest {

    @Test
    public void test1() throws Exception {
        assertThat(SherlockAnagrams.unorderedAnagramicPairs("abba".toCharArray()), is(4));
    }

    @Test
    public void test2() throws Exception {
        assertThat(SherlockAnagrams.unorderedAnagramicPairs("abcd".toCharArray()), is(0));
    }

    @Test
    public void test3() throws Exception {
        assertThat(SherlockAnagrams.unorderedAnagramicPairs("pvmupwjjjf".toCharArray()), is(6));
    }

}