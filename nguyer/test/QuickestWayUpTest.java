import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class QuickestWayUpTest {

    @Test
    public void test1() throws Exception {
        Map<Integer, Integer> shortcuts = new HashMap<>();
        shortcuts.put(32, 62);
        shortcuts.put(42, 68);
        shortcuts.put(12, 98);
        shortcuts.put(95, 13);
        shortcuts.put(97, 25);
        shortcuts.put(93, 37);
        shortcuts.put(79, 27);
        shortcuts.put(75, 19);
        shortcuts.put(49, 47);
        shortcuts.put(67, 17);
        assertThat(QuickestWayUp.solve(shortcuts), is(3));
    }

}