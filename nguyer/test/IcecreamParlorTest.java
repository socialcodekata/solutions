import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class IcecreamParlorTest {

    @Test
    public void test1() throws Exception {
        assertThat(IcecreamParlor.solve(new int[]{1, 4, 5, 3, 2}, 4), is("1 4"));
    }

    @Test
    public void test2() throws Exception {
        assertThat(IcecreamParlor.solve(new int[]{2, 2, 4, 3}, 4), is("1 2"));
    }

}