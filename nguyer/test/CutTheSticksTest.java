import org.junit.Test;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class CutTheSticksTest {
    @Test
    public void test1() throws Exception {
        assertThat(CutTheSticks.cut(new int[]{5, 4, 4, 2, 2, 8}), is(asList(6, 4, 2, 1)));
    }

}
