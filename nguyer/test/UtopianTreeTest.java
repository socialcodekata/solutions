import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class UtopianTreeTest {

    @Test
    public void test1() throws Exception {
        assertThat(UtopianTree.heightAtCycle(0), is(1));
    }

    @Test
    public void test2() throws Exception {
        assertThat(UtopianTree.heightAtCycle(1), is(2));
    }

    @Test
    public void test3() throws Exception {
        assertThat(UtopianTree.heightAtCycle(4), is(7));
    }
}
