import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class BiggerIsGreaterTest {

    @Test
    public void test1() throws Exception {
        assertThat(BiggerIsGreater.next("ab".toCharArray()), is("ba"));
    }

    @Test
    public void test2() throws Exception {
        assertThat(BiggerIsGreater.next("bb".toCharArray()), is("no answer"));
    }

    @Test
    public void test3() throws Exception {
        assertThat(BiggerIsGreater.next("hefg".toCharArray()), is("hegf"));
    }

    @Test
    public void test4() throws Exception {
        assertThat(BiggerIsGreater.next("dhck".toCharArray()), is("dhkc"));
    }

    @Test
    public void test5() throws Exception {
        assertThat(BiggerIsGreater.next("dkhc".toCharArray()), is("hcdk"));
    }

    @Test
    public void test6() throws Exception {
        assertThat(BiggerIsGreater.next("imllmmcslslkyoegymoa".toCharArray()), is("imllmmcslslkyoegyoam"));
    }
}
