import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class TheTimeInWordsTest {

    @Test
    public void test1() throws Exception {
        assertThat(TheTimeInWords.translate(5, 0), is("five o' clock"));
    }

    @Test
    public void test2() throws Exception {
        assertThat(TheTimeInWords.translate(5, 1), is("one minute past five"));
    }

    @Test
    public void test3() throws Exception {
        assertThat(TheTimeInWords.translate(5, 10), is("ten minutes past five"));
    }

    @Test
    public void test4() throws Exception {
        assertThat(TheTimeInWords.translate(5, 28), is("twenty eight minutes past five"));
    }

    @Test
    public void test5() throws Exception {
        assertThat(TheTimeInWords.translate(5, 30), is("half past five"));
    }

    @Test
    public void test6() throws Exception {
        assertThat(TheTimeInWords.translate(5, 40), is("twenty minutes to six"));
    }

    @Test
    public void test7() throws Exception {
        assertThat(TheTimeInWords.translate(5, 45), is("quarter to six"));
    }

    @Test
    public void test8() throws Exception {
        assertThat(TheTimeInWords.translate(5, 47), is("thirteen minutes to six"));
    }
}
