import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class StockMaximizeTest {

    @Test
    public void test1() throws Exception {
        assertThat(StockMaximize.solve(new int[]{5, 3, 2}), is(0L));
    }

    @Test
    public void test2() throws Exception {
        assertThat(StockMaximize.solve(new int[]{1, 2, 100}), is(197L));
    }

    @Test
    public void test3() throws Exception {
        assertThat(StockMaximize.solve(new int[]{1, 3, 1, 2}), is(3L));
    }
}
