package design.shopping;

import org.javamoney.moneta.Money;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ShoppingTest {

    private Checkout co = new Checkout(new PricingRules());

    @Test
    public void test1() throws Exception {
        co.scan("atv");
        co.scan("atv");
        co.scan("atv");
        co.scan("vga");

        assertThat(co.total(), is(Money.of(249, "USD")));
    }

    @Test
    public void test2() throws Exception {
        co.scan("atv");
        co.scan("ipd");
        co.scan("ipd");
        co.scan("atv");
        co.scan("ipd");
        co.scan("ipd");
        co.scan("ipd");

        assertThat(co.total(), is(Money.of(2718.95, "USD")));
    }

    @Test
    public void test3() throws Exception {
        co.scan("mbp");
        co.scan("vga");
        co.scan("ipd");

        assertThat(co.total(), is(Money.of(1949.98, "USD")));
    }
}
