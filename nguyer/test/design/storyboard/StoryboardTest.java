package design.storyboard;

import org.junit.Test;

import static design.storyboard.Story.State.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class StoryboardTest {

    @Test
    public void shouldOnlyAllowStoryToMoveToAdjacentSwimlanes() {
        Story createUser = new Story("Create a user", 8);

        // Forwards
        createUser.moveToSwimlane(InDev);
        createUser.moveToSwimlane(CodeReview);
        createUser.moveToSwimlane(QA);
        createUser.moveToSwimlane(Done);

        // And backwards
        createUser.moveToSwimlane(QA);
        createUser.moveToSwimlane(CodeReview);
        createUser.moveToSwimlane(InDev);
        createUser.moveToSwimlane(ReadyForDev);

        // But no skipping
        try {
            createUser.moveToSwimlane(Done);
            fail();
        } catch (Exception expected) {
        }
    }
    
    @Test
    public void shouldThrowExceptionWhenAddingStoriesToAClosedIteration() {
        Iteration iterationOne = new Iteration();
        iterationOne.close();

        Story createUser = new Story("Create a user", 8);
        try {
            iterationOne.add(createUser);
            fail();
        } catch (Exception expected) {
        }
    }

    @Test
    public void testPointsCalculation() throws Exception {
        Project project = new Project();

        Iteration iterationOne = new Iteration();
        Iteration iterationTwo = new Iteration();
        project.addIteration(iterationOne);
        project.addIteration(iterationTwo);

        Story createUser = new Story("Create a user", 8);
        iterationOne.add(createUser);

        Story deleteUser = new Story("Delete a user", 12);
        Story updateUser = new Story("Update a user", 1);
        iterationTwo.add(deleteUser);
        iterationTwo.add(updateUser);

        // Regular points calculation
        assertThat(iterationOne.getTotalPoints(), is(8));
        assertThat(iterationTwo.getTotalPoints(), is(13));

        assertThat(project.getTotalPoints(), is(21));

        // Done points calculation
        assertThat(iterationTwo.getTotalDonePoints(), is(0));
        moveToDone(updateUser);
        assertThat(iterationTwo.getTotalDonePoints(), is(1));
        assertThat(project.getTotalDonePoints(), is(1));

    }

    private void moveToDone(Story story) {
        story.moveToSwimlane(InDev);
        story.moveToSwimlane(CodeReview);
        story.moveToSwimlane(QA);
        story.moveToSwimlane(Done);
    }
}
