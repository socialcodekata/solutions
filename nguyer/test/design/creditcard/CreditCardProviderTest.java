package design.creditcard;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class CreditCardProviderTest {

    private CreditCardProvider provider = new CreditCardProvider();

    @Test
    public void test() throws Exception {
        provider.process("Add Tom 4111111111111111 $1000");
        provider.process("Add Lisa 5454545454545454 $3000");
        provider.process("Add Quincy 1234567890123456 $2000");
        provider.process("Charge Tom $500");
        provider.process("Charge Tom $800");
        provider.process("Charge Lisa $7");
        provider.process("Credit Lisa $100");
        provider.process("Credit Quincy $200");

        assertThat(provider.getSummary(), is(
                "Lisa: $-93\n" +
                "Quincy: error\n" +
                "Tom: $500\n"
        ));
    }
}
