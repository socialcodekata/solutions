import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class FibonacciModifiedTest {

    @Test
    public void test1() throws Exception {
        assertThat(FibonacciModified.computeRecursiveMemo(0, 1, 5), is("5"));
    }

    @Test
    public void test2() throws Exception {
        assertThat(FibonacciModified.computeRecursiveMemo(0, 1, 10), is("84266613096281243382112"));
    }
}
