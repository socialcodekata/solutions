import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class RestaurantTest {

    @Test
    public void test1() throws Exception {
        assertThat(Restaurant.cut(2, 2), is(1));
    }

    @Test
    public void test2() throws Exception {
        assertThat(Restaurant.cut(6, 9), is(6));
    }

}