import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class CoinOnTheTableTest {

    @Test
    public void test1() throws Exception {
        assertThat(CoinOnTheTable.findMinFlips(new char[][]{{'R', 'D'}, {'*', 'L'}}, 3), is(0));
    }

    @Test
    public void test2() throws Exception {
        assertThat(CoinOnTheTable.findMinFlips(new char[][]{{'R', 'D'}, {'*', 'L'}}, 1), is(1));
    }

    @Test
    public void test3() throws Exception {
        char[][] board = {
                "DRLULLRRRLRDLRRLU".toCharArray(),
                "URUURLRLLLDULRRRR".toCharArray(),
                "DRDLDDLDRRDRURRLR".toCharArray(),
                "DRRRRRRLDULDDUDLD".toCharArray(),
                "DULRDDULDUDULRDUD".toCharArray(),
                "LRURUDURRUURDDUDL".toCharArray(),
                "URURDLRUULRRDLDLR".toCharArray(),
                "DLRUDLDRUUDULUDUU".toCharArray(),
                "*ULLURDRDUURLDRDR".toCharArray(),
                "ULDUDUUULLURURURR".toCharArray(),
                "LDRDLDRRLDDRRLRLD".toCharArray(),
                "RDDLURRRDDLRDURLD".toCharArray(),
                "ULRLRLRRLLLRRURRL".toCharArray(),
                "RLLLDRDURLRURLUDD".toCharArray(),
                "DRLDURRLURUULLRDU".toCharArray(),
                "RURRUDLLLDDDRUUUD".toCharArray(),
                "UUUUDDLRURULRRDRD".toCharArray(),
                "URDUUDRDLDRLLULRU".toCharArray(),
                "DRDUUULUUDURULDDL".toCharArray(),
                "LLULDRLRRRUDLDRRU".toCharArray()
        };
        assertThat(CoinOnTheTable.findMinFlips(board, 47), is(3));
    }

}