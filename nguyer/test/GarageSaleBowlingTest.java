import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class GarageSaleBowlingTest {
    @Test
    public void test1() throws Exception {
        assertThat(GarageSaleBowling.totalScore(new String[]{
                "5",
                "-2",
                "4",
                "Z",
                "X",
                "9",
                "+",
                "+"
        }), is(27));
    }
}
