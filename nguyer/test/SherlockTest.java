import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class SherlockTest {

    @Test
    public void test1() throws Exception {
        assertThat(Sherlock.getRotatedElement(3, 2, new int[]{1, 2, 3}, 0), is(2));
    }

    @Test
    public void test2() throws Exception {
        assertThat(Sherlock.getRotatedElement(3, 2, new int[]{1, 2, 3}, 1), is(3));
    }

    @Test
    public void test3() throws Exception {
        assertThat(Sherlock.getRotatedElement(3, 2, new int[]{1, 2, 3}, 2), is(1));
    }

}