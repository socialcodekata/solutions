import java.math.BigInteger;
import java.util.Scanner;

public class FibonacciModified {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println(computeIterative(in.nextInt(), in.nextInt(), in.nextInt()));
    }


    public static String computeIterative(long a, long b, int n) {
        if (n == 1) return Long.toString(a);
        if (n == 2) return Long.toString(b);
        BigInteger bigA = BigInteger.valueOf(a);
        BigInteger bigB = BigInteger.valueOf(b);
        BigInteger bigC = BigInteger.ZERO;
        for (int i = 3; i <= n; i++) {
            bigC = bigB.pow(2).add(bigA);
            bigA = bigB;
            bigB = bigC;
        }
        return bigC.toString();
    }

    public static String computeRecursive(long a, long b, int n) {
        BigInteger bigA = BigInteger.valueOf(a);
        BigInteger bigB = BigInteger.valueOf(b);
        return computeRecursive(bigA, bigB, n).toString();
    }

    private static BigInteger computeRecursive(BigInteger a, BigInteger b, int n) {
        if (n == 1) return a;
        if (n == 2) return b;
        return computeRecursive(a, b, n - 1).pow(2).add(computeRecursive(a, b, n - 2));
    }

    public static String computeRecursiveMemo(long a, long b, int n) {
        BigInteger[] memo = new BigInteger[n + 1];
        memo[1] = BigInteger.valueOf(a);
        memo[2] = BigInteger.valueOf(b);
        return computeRecursiveMemo(memo, n).toString();
    }

    private static BigInteger computeRecursiveMemo(BigInteger[] memo, int n) {
        if (memo[n] != null) return memo[n];
        memo[n] = computeRecursiveMemo(memo, n - 1).pow(2).add(computeRecursiveMemo(memo, n - 2));
        return memo[n] = computeRecursiveMemo(memo, n - 1).pow(2).add(computeRecursiveMemo(memo, n - 2));
    }
}
