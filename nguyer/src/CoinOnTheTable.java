import java.util.Scanner;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Math.min;

/**
 * https://www.hackerrank.com/challenges/coin-on-the-table
 */
public class CoinOnTheTable {

    private static Integer[][][] cache; // memoize results for efficiency

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int k = in.nextInt();

        char[][] board = new char[n][m];
        for (int i = 0; i < n; i++) {
            board[i] = in.next().toCharArray();
        }

        int minFlips = findMinFlips(board, k);
        System.out.println((minFlips == MAX_VALUE) ? -1 : minFlips);
    }

    public static int findMinFlips(char[][] board, int stepQuota) {
        cache = new Integer[51][51][1000];
        return minFlipsFromPosition(board, 0, 0, stepQuota);
    }

    /**
     * @return minimum number of flips to reach the *
     * Integer.MAX_VALUE if it's not possible to reach the * within stepsRemaining
     */
    private static int minFlipsFromPosition(char[][] board, int row, int col, int stepsRemaining) {
        if (board[row][col] == '*') {
            return 0;
        }
        if (stepsRemaining == 0) {
            return MAX_VALUE;
        }
        if (cache[row][col][stepsRemaining] != null) {
            return cache[row][col][stepsRemaining];
        }

        int best = MAX_VALUE;
        if (row < board.length - 1) { // not on the last row, walk down
            int flips = minFlipsFromPosition(board, row + 1, col, stepsRemaining - 1);
            if (board[row][col] != 'D' && flips != MAX_VALUE) flips++;
            best = min(best, flips);
        }
        if (col < board[0].length - 1) { // not on the last column, walk right
            int flips = minFlipsFromPosition(board, row, col + 1, stepsRemaining - 1);
            if (board[row][col] != 'R' && flips != MAX_VALUE) flips++;
            best = min(best, flips);
        }
        if (row > 0) { // not on the first row, walk up
            int flips = minFlipsFromPosition(board, row - 1, col, stepsRemaining - 1);
            if (board[row][col] != 'U' && flips != MAX_VALUE) flips++;
            best = min(best, flips);
        }
        if (col > 0) { // not on the first column, walk left
            int flips = minFlipsFromPosition(board, row, col - 1, stepsRemaining - 1);
            if (board[row][col] != 'L' && flips != MAX_VALUE) flips++;
            best = min(best, flips);
        }

        cache[row][col][stepsRemaining] = best;

        return best;
    }

}