import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/stockmax
 */
public class StockMaximize {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testCases = in.nextInt();
        for (int i = 0; i < testCases; i++) {
            int days = in.nextInt();
            int[] prices = new int[days];
            for (int j = 0; j < days; j++) {
                prices[j] = in.nextInt();
            }
            System.out.println(solve(prices));
        }
    }

    public static long solve(int[] prices) {
        long profit = 0;
        long localMaximum = prices[prices.length - 1];
        for (int i = prices.length - 1; i > 0; i--) {
            if (prices[i] > localMaximum) {
                localMaximum = prices[i];
            }
            long profitToBeMadeToday = localMaximum - prices[i - 1];
            if (profitToBeMadeToday > 0) {
                profit += profitToBeMadeToday;
            }
        }
        return profit;
    }
}
