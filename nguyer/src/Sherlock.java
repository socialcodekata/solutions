import java.util.Scanner;

import static java.lang.Math.floorMod;

/**
 * https://www.hackerrank.com/challenges/sherlock-and-watson
 */
public class Sherlock {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int q = in.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }

        for (int i = 0; i < q; i++) {
            int index = in.nextInt();
            System.out.println(getRotatedElement(n, k, arr, index));
        }
    }

    public static int getRotatedElement(int size, int rotations, int[] arr, int index) {
        return arr[floorMod((index - rotations), size)];
    }
}
