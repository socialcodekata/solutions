import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.String.join;

/**
 * https://www.hackerrank.com/challenges/the-time-in-words
 */
public class TheTimeInWords {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int hours = in.nextInt();
        int minutes = in.nextInt();
        System.out.println(translate(hours, minutes));
    }

    public static String translate(int hours, int minutes) {
        List<String> parts = new ArrayList<>();
        if (minutes == 0) {
            parts.add(ONES[hours]);
            parts.add("o' clock");
        } else {
            if (minutes == 30) {
                parts.add("half");
            } else if (minutes == 15 || minutes == 45) {
                parts.add("quarter");
            } else if (minutes < 30) {
                min(minutes, parts);
            } else if (minutes > 30) {
                min(60 - minutes, parts);
            }

            if (minutes <= 30) {
                parts.add("past");
                parts.add(ONES[hours]);
            } else {
                parts.add("to");
                parts.add(ONES[hours + 1]);
            }
        }
        return join(" ", parts);
    }

    private static void min(int minutes, List<String> parts) {
        if (minutes >= 20) {
            parts.add(TENS[minutes / 10]);
            if (minutes > 20) {
                parts.add(ONES[minutes % 10]);
            }
        } else {
            parts.add(ONES[minutes]);
        }

        if (minutes == 1) {
            parts.add("minute");
        } else {
            parts.add("minutes");
        }
    }

    private static final String[] ONES = {
            "zero",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten",
            "eleven",
            "twelve",
            "thirteen",
            "fourteen",
            "fifteen",
            "sixteen",
            "seventeen",
            "eighteen",
            "nineteen"
    };

    private static final String[] TENS = {
            "zero",
            "ten",
            "twenty",
            "thirty",
            "forty",
            "fifty",
            "sixty",
            "seventy",
            "eighty",
            "ninety",
    };
}
