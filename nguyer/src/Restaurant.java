import java.util.Scanner;

import static java.lang.Math.max;

/**
 * https://www.hackerrank.com/challenges/restaurant
 */
public class Restaurant {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testCases = in.nextInt();
        for (int i = 0; i < testCases; i++) {
            int length = in.nextInt();
            int breadth = in.nextInt();
            System.out.println(cut(length, breadth));
        }
    }

    public static int cut(int length, int breadth) {
        int sideLength = max(length, breadth);
        while (sideLength > 0) {
            if ((length % sideLength == 0) && (breadth % sideLength == 0)) {
                break;
            }
            sideLength--;
        }
        return (length / sideLength) * (breadth / sideLength);
    }
}
