package design.storyboard;

import java.util.ArrayList;
import java.util.List;

import static design.storyboard.Iteration.State.Closed;
import static design.storyboard.Iteration.State.Open;

public class Iteration {

    private List<Story> stories = new ArrayList<>();
    private State state;

    public Iteration() {
        this.state = Open;
    }

    public void add(Story story) {
        if (state == Closed) {
            throw new IllegalStateException();
        }
        stories.add(story);
    }

    public int getTotalPoints() {
        return stories
                .stream()
                .mapToInt(Story::getPoints)
                .sum();
    }

    public int getTotalDonePoints() {
        return stories
                .stream()
                .filter(Story::isDone)
                .mapToInt(Story::getPoints)
                .sum();
    }

    public void close() {
        this.state = Closed;
    }

    public enum State {
        Open,
        Closed
    }

}
