package design.storyboard;

import java.util.ArrayList;
import java.util.List;

public class Project {

    private List<Iteration> iterations = new ArrayList<>();

    public void addIteration(Iteration iteration) {
        iterations.add(iteration);
    }

    public int getTotalPoints() {
        return iterations
                .stream()
                .mapToInt(Iteration::getTotalPoints)
                .sum();
    }

    public int getTotalDonePoints() {
        return iterations
                .stream()
                .mapToInt(Iteration::getTotalDonePoints)
                .sum();
    }
}
