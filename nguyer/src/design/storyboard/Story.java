package design.storyboard;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static design.storyboard.Story.State.Done;
import static design.storyboard.Story.State.ReadyForDev;
import static java.time.LocalDateTime.now;

public class Story {

    private final String description;
    private final int points;
    private State state;
    private Map<LocalDateTime, State> audit = new HashMap<>();

    public Story(String description, int points) {
        this.description = description;
        this.points = points;
        this.state = ReadyForDev;
    }

    public void moveToSwimlane(State nextState) {
        if (Math.abs(state.ordinal() - nextState.ordinal()) > 1) {
            throw new IllegalArgumentException("Cannot skip from " + state + " to " + nextState);
        }
        state = nextState;
        audit.put(now(), state);
    }

    public int getPoints() {
        return points;
    }

    public boolean isDone() {
        return state == Done;
    }

    public enum State {
        ReadyForDev,
        InDev,
        CodeReview,
        QA,
        Done
    }
}
