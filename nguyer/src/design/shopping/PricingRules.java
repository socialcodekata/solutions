package design.shopping;

import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static design.shopping.Checkout.CCY;

public class PricingRules {
    private Map<String, PricingRule> rules = new HashMap<>();

    public PricingRules() {
        // the brand new Super iPad will have a bulk discounted applied, where the price will drop to $499.99 each, if someone buys more than 4
        rules.put("ipd", new BulkDiscount(Money.of(549.99, CCY), 4, "ipd", Money.of(499.99, CCY)));

        rules.put("mbp", new FullPrice(Money.of(1399.99, CCY)));

        // we're going to have a 3 for 2 deal on Apple TVs. For example, if you buy 3 Apple TVs, you will pay the price of 2 only
        rules.put("atv", new NthItemFree(Money.of(109.50, CCY), 3));

        // we will bundle in a free VGA adapter free of charge with every MacBook Pro sold
        rules.put("vga", new FreeBundle(Money.of(30, CCY), "mbp"));
    }

    public MonetaryAmount calculate(List<String> cart) {
        MonetaryAmount total = Money.of(0, CCY);
        for (String sku : cart) {
            total = total.add(rules.get(sku).apply(cart));
        }
        return total;
    }

    private interface PricingRule {
        MonetaryAmount apply(List<String> cart);
    }

    private class NthItemFree implements PricingRule {
        private MonetaryAmount regularPrice;
        private int n;
        private int itemsBought = 0;

        public NthItemFree(MonetaryAmount regularPrice, int n) {
            this.regularPrice = regularPrice;
            this.n = n;
        }

        public MonetaryAmount apply(List<String> cart) {
            itemsBought++;
            if (itemsBought % n == 0) {
                return Money.of(0, CCY);
            } else {
                return regularPrice;
            }
        }
    }

    private class BulkDiscount implements PricingRule {
        private MonetaryAmount regularPrice;
        private int qtyRequired;
        private String sku;
        private MonetaryAmount discountPrice;

        public BulkDiscount(MonetaryAmount regularPrice, int qtyRequired, String sku, MonetaryAmount discountPrice) {
            this.regularPrice = regularPrice;
            this.qtyRequired = qtyRequired;
            this.sku = sku;
            this.discountPrice = discountPrice;
        }

        public MonetaryAmount apply(List<String> cart) {
            long count = cart.stream().filter(item -> item.equals(sku)).count();

            if (count > qtyRequired) {
                return discountPrice;
            } else {
                return regularPrice;
            }
        }
    }

    private class FreeBundle implements PricingRule {
        private final MonetaryAmount regularPrice;
        private final String prerequisiteProduct;
        private int itemsBought = 0;

        public FreeBundle(MonetaryAmount regularPrice, String prerequisiteProduct) {
            this.regularPrice = regularPrice;
            this.prerequisiteProduct = prerequisiteProduct;
        }

        public MonetaryAmount apply(List<String> cart) {
            itemsBought++;
            long prerequisiteProductCount = cart.stream().filter(item -> item.equals(prerequisiteProduct)).count();
            if (itemsBought <= prerequisiteProductCount) {
                return Money.of(0, CCY);
            } else {
                return regularPrice;
            }
        }
    }

    private class FullPrice implements PricingRule {
        private MonetaryAmount regularPrice;

        public FullPrice(MonetaryAmount regularPrice) {
            this.regularPrice = regularPrice;
        }

        public MonetaryAmount apply(List<String> cart) {
            return regularPrice;
        }
    }
}
