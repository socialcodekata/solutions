package design.shopping;

import javax.money.MonetaryAmount;
import java.util.ArrayList;
import java.util.List;

public class Checkout {

    public static final String CCY = "USD";

    private final List<String> cart;
    private final PricingRules pricingRules;

    public Checkout(PricingRules pricingRules) {
        this.pricingRules = pricingRules;
        this.cart = new ArrayList<>();
    }

    public void scan(String sku) {
        cart.add(sku);
    }

    public MonetaryAmount total() {
        return pricingRules.calculate(cart);
    }
}
