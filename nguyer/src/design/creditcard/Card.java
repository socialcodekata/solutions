package design.creditcard;

import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;

public class Card {

    private final long number;
    private final int limit;
    private int balance;

    public Card(String cardNumberStr, int limit) {
        if (!isValidLuhn(cardNumberStr)) {
            throw new IllegalArgumentException(cardNumberStr + " not a valid card number");
        }
        if (cardNumberStr.length() > 19) {
            throw new IllegalArgumentException(cardNumberStr + " is too long");
        }
        this.number = parseLong(cardNumberStr);
        this.limit = limit;
        this.balance = 0;
    }

    public void charge(int amount) {
        if (balance + amount < limit) {
            balance += amount;
        }
    }

    public void credit(int amount) {
        balance -= amount;
    }

    public String balanceString() {
        return "$" + balance;
    }

    private boolean isValidLuhn(String ccNumber) {
        int sum = 0;
        boolean alternate = false;
        for (int i = ccNumber.length() - 1; i >= 0; i--) {
            int n = parseInt(ccNumber.substring(i, i + 1));
            if (alternate) {
                n *= 2;
                if (n > 9) {
                    n = (n % 10) + 1;
                }
            }
            sum += n;
            alternate = !alternate;
        }
        return (sum % 10 == 0);
    }
}
