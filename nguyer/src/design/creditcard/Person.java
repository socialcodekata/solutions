package design.creditcard;

public class Person {

    private String name;
    private Card card;

    private static final String ERROR = "error";

    public Person(String name) {
        this.name = name;
    }

    public void associate(Card card) {
        this.card = card;
    }

    public void charge(int amount) {
        if (card != null) {
            card.charge(amount);
        }
    }

    public void credit(int amount) {
        if (card != null) {
            card.credit(amount);
        }
    }

    public String getSummary() {
        String balance = (card != null) ? card.balanceString() : ERROR;
        return name + ": " + balance;
    }
}
