package design.creditcard;

import java.util.Map;
import java.util.TreeMap;

import static java.lang.Integer.parseInt;

public class CreditCardProvider {

    Map<String, Person> personRepository = new TreeMap<>(); // Treemap naturally sorts by keys

    public void process(String input) {
        if (input == null) {
            throw new IllegalArgumentException("Null input");
        }

        String[] args = input.split(" ");
        switch (args[0]) {
            case "Add":
                if (args.length < 4) {
                    throw new IllegalArgumentException(args[0] + " command needs more arguments: " + input);
                }
                add(args[1], args[2], args[3]);
                break;
            case "Charge":
                if (args.length < 3) {
                    throw new IllegalArgumentException(args[0] + " command needs more arguments: " + input);
                }
                charge(args[1], args[2]);
                break;
            case "Credit":
                if (args.length < 3) {
                    throw new IllegalArgumentException(args[0] + " command needs more arguments: " + input);
                }
                credit(args[1], args[2]);
                break;
            default:
                throw new IllegalArgumentException("Unrecognised command: " + args[0]);
        }
    }

    public String getSummary() {
        StringBuilder summary = new StringBuilder();
        for (Person person : personRepository.values()) {
            summary.append(person.getSummary()).append("\n");
        }
        return summary.toString();
    }

    private void add(String name, String cardNumber, String cardLimit) {
        personRepository.putIfAbsent(name, new Person(name));
        Person person = personRepository.get(name);


        Card card;
        try {
            card = new Card(cardNumber, parseMonetaryAmount(cardLimit));
        } catch (Exception e) {
            // log e
            return;
        }
        person.associate(card);
    }

    private void charge(String name, String amount) {
        Person person = personRepository.get(name);
        if (person == null) {
            return;
        }
        person.charge(parseMonetaryAmount(amount));
    }

    private void credit(String name, String amount) {
        Person person = personRepository.get(name);
        if (person == null) {
            return;
        }
        person.credit(parseMonetaryAmount(amount));
    }

    private static int parseMonetaryAmount(String monetaryAmount) {
        return parseInt(monetaryAmount.replace("$", ""));
    }
}
