import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static java.util.Arrays.copyOfRange;
import static java.util.Arrays.sort;

/**
 * https://www.hackerrank.com/challenges/sherlock-and-anagrams
 */
public class SherlockAnagrams {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.println(unorderedAnagramicPairs(in.next().toCharArray()));
        }
    }

    public static int unorderedAnagramicPairs(char[] chars) {
        Map<String, Integer> occurrencesMap = new HashMap<>();
        int anagramicPairs = 0;
        // Lengths of substrings to check, starting with individual characters, then pairs, then tuples
        for (int length = 1; length < chars.length; length++) {
            // Walk down the list, testing each substring of [j,j+length]
            for (int j = 0; j <= chars.length - length; j++) {
                char[] substr = copyOfRange(chars, j, j + length);
                sort(substr);
                String s = new String(substr);
                if (occurrencesMap.containsKey(s)) {
                    int occurrences = occurrencesMap.get(s);

                    /*
                     * 1 occurrence = 0 anagramic pairs
                     * 2 occurrences = 1 anagramic pairs
                     * 3 occurrences = 3 anagramic pairs
                     * 4 occurrences = 6 anagramic pairs
                     * 5 occurrences = 10 anagramic pairs
                     *
                     * When a new occurrence is added, the number of anagramic pairs increases by
                     * the number of occurrences already present
                     */
                    anagramicPairs += occurrences;
                    occurrencesMap.put(s, occurrences + 1);
                } else {
                    occurrencesMap.put(s, 1);
                }
            }
        }
        return anagramicPairs;
    }
}
