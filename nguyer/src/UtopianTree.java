import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/utopian-tree
 */
public class UtopianTree {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            int cycles = in.nextInt();
            System.out.println(heightAtCycle(cycles));
        }
    }

    public static int heightAtCycle(int cycles) {
        int height = 1;
        for (int i = 1; i <= cycles; i++) {
            height = (i % 2 == 1) ? (height * 2) : (height + 1);
        }
        return height;
    }
}
