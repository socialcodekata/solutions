import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class GarageSaleBowling {

    public static int totalScore(String[] balls) {
        Deque<Integer> scores = new ArrayDeque<>();
        for (String ball : balls) {
            switch (ball) {
                case "Z":
                    scores.pop();
                    break;
                case "X":
                    scores.push(scores.peek() * 2);
                    break;
                case "+":
                    int last = scores.pop(); // Modifies stack
                    int secondLast = scores.peek();
                    scores.push(last); // Restores stack to previous state
                    scores.push(secondLast + last);
                    break;
                default:
                    scores.push(Integer.parseInt(ball));
                    break;
            }
        }

        int total = 0;
        for (int score : scores) {
            total += score;
        }
        return total;
    }

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        final String fileName = System.getenv("OUTPUT_PATH");
        BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        int res;

        int _balls_size = 0;
        _balls_size = Integer.parseInt(in.nextLine());
        String[] _balls = new String[_balls_size];
        String _balls_item;
        for (int _balls_i = 0; _balls_i < _balls_size; _balls_i++) {
            try {
                _balls_item = in.nextLine();
            } catch (Exception e) {
                _balls_item = null;
            }
            _balls[_balls_i] = _balls_item;
        }

        res = totalScore(_balls);
        bw.write(String.valueOf(res));
        bw.newLine();

        bw.close();
    }
}
