import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/icecream-parlor
 */
public class IcecreamParlor {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testCases = in.nextInt();
        for (int i = 0; i < testCases; i++) {
            int budget = in.nextInt();
            int numFlavours = in.nextInt();
            int[] flavours = new int[numFlavours];
            for (int j = 0; j < numFlavours; j++) {
                flavours[j] = in.nextInt();
            }
            System.out.println(solve(flavours, budget));
        }
    }

    public static String solve(int[] flavours, int target) {
        Map<Integer, Integer> costsToIndices = new HashMap<>();

        for (int i = 0; i < flavours.length; i++) {
            int remainder = target - flavours[i];
            if (costsToIndices.containsKey(remainder)) {
                return (costsToIndices.get(remainder) + 1) + " " + (i + 1);
            } else {
                costsToIndices.put(flavours[i], i);
            }
        }
        return "";
    }

}
