import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/bigger-is-greater
 */
public class BiggerIsGreater {

    private static final String NO_ANSWER = "no answer";

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.println(next(in.next().toCharArray()));
        }
    }

    public static String next(char[] original) {
        /*
         * Scanning backwards, find first char not in ascending order
         * e.g. aaaahzyxpcba
         *          ^
         *          charToSwap
         */
        int charToSwap = -1;
        for (int i = original.length - 1; i > 0; i--) {
            if (original[i - 1] < original[i]) {
                charToSwap = i - 1;
                break;
            }
        }
        if (charToSwap < 0) return NO_ANSWER;

        /*
         * Scanning backwards, find first char greater than charToSwap
         * e.g. aaaahzyxpcba
         *              ^
         *              swapWith (p > h)
         */
        int swapWith = -1;
        for (int i = original.length - 1; i > 0; i--) {
            if (original[i] > original[charToSwap]) {
                swapWith = i;
                break;
            }
        }
        if (swapWith < 0) return NO_ANSWER;

        /*
         * Swap the two chars
         * e.g. aaaahzyxpcba
         *      aaaapzyxhcba
         */
        char tmp = original[charToSwap];
        original[charToSwap] = original[swapWith];
        original[swapWith] = tmp;

        /*
         * Reverse the subarray from charToSwap+1 onwards. This subarray will always be in descending order
         * e.g. aaaapzyxhcba
         *      aaaapabchxyz
         */
        reverseInPlace(original, charToSwap + 1, original.length - 1);
        return new String(original);
    }

    private static void reverseInPlace(char[] original, int begin, int end) {
        char temp;
        while (end > begin) {
            temp = original[begin];
            original[begin] = original[end];
            original[end] = temp;
            end--;
            begin++;
        }
    }
}
