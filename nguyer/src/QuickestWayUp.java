import java.util.*;
import java.util.Map.Entry;

/**
 * https://www.hackerrank.com/challenges/the-quickest-way-up
 */
public class QuickestWayUp {

    private static final int SIZE = 100;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            Map<Integer, Integer> shortcuts = new HashMap<>();
            int ladders = in.nextInt();
            for (int j = 0; j < ladders; j++) {
                shortcuts.put(in.nextInt(), in.nextInt());
            }
            int snakes = in.nextInt();
            for (int j = 0; j < snakes; j++) {
                shortcuts.put(in.nextInt(), in.nextInt());
            }

            System.out.println(solve(shortcuts));
        }
    }

    public static int solve(Map<Integer, Integer> shortcuts) {
        Square[] board = buildBoard(shortcuts);

        // bfs
        board[1].steps = 0;
        Queue<Square> queue = new ArrayDeque<>();
        queue.add(board[1]);
        while (!queue.isEmpty()) {
            Square square = queue.remove();

            square.paths.stream()
                    .filter(s -> !s.visited) // break graph cycles
                    .forEach(s -> {
                        s.steps = square.steps + 1;
                        s.visited = true;
                        queue.add(s);
                    });
        }

        return board[100].steps;
    }

    private static Square[] buildBoard(Map<Integer, Integer> shortcuts) {
        Square[] board = new Square[SIZE + 1];
        for (int square = 1; square <= SIZE; square++) {
            board[square] = new Square();
        }

        // build snakes and ladders
        // if there's a ladder from 6 -> 25, then square 6 ceases to exist and is effectively *replaced* by square 25
        for (Entry<Integer, Integer> shortcut : shortcuts.entrySet()) {
            board[shortcut.getKey()] = board[shortcut.getValue()];
        }

        // connect non-shortcut squares based on regular die rolls
        for (int square = 1; square <= SIZE; square++) {
            if (shortcuts.containsKey(square)) {
                continue;
            }

            for (int roll = 1; roll <= 6; roll++) {
                if (square + roll <= SIZE) {
                    board[square].paths.add(board[square + roll]);
                }
            }
        }
        return board;
    }

    public static class Square {
        public boolean visited;
        public List<Square> paths = new ArrayList<>();
        public int steps = -1;
    }
}
