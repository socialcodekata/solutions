import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.util.Arrays.sort;

public class CutTheSticks {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        int[] sticks = new int[n];
        for (int i = 0; i < n; i++) {
            sticks[i] = in.nextInt();
        }

        cut(sticks).forEach(System.out::println);
    }

    public static List<Integer> cut(int[] sticks) {
        sort(sticks);
        List<Integer> result = new ArrayList<>();
        int sticksRemoved = 0;
        for (int i = 0; i < sticks.length; i++) {
            sticksRemoved++;
            if (i == sticks.length - 1
                    || sticks[i] != sticks[i + 1]) {
                result.add(sticks.length - 1 - i + sticksRemoved);
                sticksRemoved = 0;
            }
        }
        return result;
    }
}
