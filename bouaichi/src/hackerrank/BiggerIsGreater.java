package hackerrank;

import java.util.Arrays;
import java.util.Scanner;

public class BiggerIsGreater {
    private String characters;

    public BiggerIsGreater(String characters) {
        this.characters = characters;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int nbOfTests = in.nextInt();
        for (int i = 0; i < nbOfTests; i++) {
            String word = in.next();
            System.out.println(new BiggerIsGreater(word).calculateBIG());
        }
        in.close();
    }

    public String calculateBIG() {
        String result = "";
        char[] i = characters.toCharArray();
        int pivot = getIndexOfPivot(i);
        char immediatelyGreater = getCharImmediatelyGreater(
                Arrays.copyOfRange(i, pivot, i.length), i[pivot]);
        char[] oldRest = Arrays.copyOfRange(i, pivot, i.length);
        if (immediatelyGreater == i[pivot] && pivot == 0) {
            return "no answer";
        }
        replace(oldRest, immediatelyGreater, i[pivot]);
        Arrays.sort(oldRest);
        char[] newRest = removeElementFromArray(oldRest, i[pivot]);
        char[] r = new char[i.length];
        for (int j=0; j < i.length; j++) {
            if (j < pivot) {
                r[j] = i[j];
            } else if (j == pivot) {
                r[j] = immediatelyGreater;
            } else {
                r[j] = newRest[j - pivot - 1];
            }
        }
        result = new String(r);
        if (result.equals(characters)) {
            result = "no answer";
        }
        return result;
    }

    private int getIndexOfPivot(char[] array) {
        for (int i = array.length - 1; i > 0; i--) {
            if (array[i] > array[i-1]) {
                return i-1;
            }
        }
        return 0;
    }

    private char getCharImmediatelyGreater(char[] in, char c) {
        Arrays.sort(in);
        for (int i = 0; i < in.length; i++) {
            if (c < in[i]) {
                return in[i];
            }
        }
        return c;
    }

    private void replace(char[] in, char c, char with) {
        for (int i = 0; i < in.length; i++) {
            if (in[i] == c) {
                in[i] = with;
                break;
            }
        }
    }

    private char[] removeElementFromArray(char[] in, char elt) {
        char[] out = new char[in.length - 1];
        int outIdx = 0;
        boolean skippedOnce = false;
        for (int i = 0; i < in.length; i++) {
            if (!skippedOnce && in[i] == elt) {
                skippedOnce = true;
                continue;
            }
            out[outIdx] = in[i];
            outIdx++;
        }
        return out;
    }
}
