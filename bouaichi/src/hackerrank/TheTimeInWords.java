package hackerrank;

import java.util.Scanner;

public class TheTimeInWords {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int hours = in.nextInt();
        int minutes = in.nextInt();
        System.out.println(new TheTimeInWords().translate(hours, minutes));
        in.close();
    }
    private static String[] VALUES = {
        "o' clock",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
        "ten",
        "eleven",
        "twelve",
        "thirteen",
        "fourteen",
        "quarter",
        "sixteen",
        "seventeen",
        "eighteen",
        "nineteen",
        "twenty",
        "twenty one",
        "twenty two",
        "twenty three",
        "twenty four",
        "twenty five",
        "twenty six",
        "twenty seven",
        "twenty eight",
        "twenty nine",
        "half"
    };

    public String translate(int hours, int minutes) {
        String past = (minutes <= 30)?"past" : "to";
        if (minutes > 30) {
            minutes = (60 - minutes);
            hours++;
        }
        String format;
        if (minutes % 60 == 0) {
            format = "[hours] [minutes]";
        } else if (minutes % 15 == 0) {
            format = "[minutes] [past] [hours]";
        } else if (minutes == 1){
            format = "[minutes] minute [past] [hours]";
        } else {
            format = "[minutes] minutes [past] [hours]";
        }
        return format.replace("[hours]", VALUES[hours]).replace("[minutes]", VALUES[minutes]).replace("[past]", past);
    }
}
