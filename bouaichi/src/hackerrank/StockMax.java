package hackerrank;

import java.util.Scanner;

public class StockMax {
    private int[] predictions;

    public StockMax(int[] predictions) {
        this.predictions = predictions;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int nbOfTests = in.nextInt();
        for (int i = 0; i < nbOfTests; i++) {
            int size = in.nextInt();
            int[] predictions = new int[size];
            for (int j = 0; j < size; j++) {
                predictions[j] = in.nextInt();
            }
            System.out.println(new StockMax(predictions).calculateMaxGain());
        }
        in.close();
    }

    public long calculateMaxGain() {
        long gain = 0;
        int max = 0;
        for (int i = predictions.length - 1; i >= 0; i--) {
            if (predictions[i] < max) {
                gain += (max - predictions[i]);
            } else {
                max = predictions[i];
            }
        }
        return gain;
    }
}
