package hackerrank;

import static org.junit.Assert.*;

import org.junit.Test;


public class TimeInWordsTest {

    @Test
    public void test1() throws Exception {
        String time = new TheTimeInWords().translate(5, 0);
        assertEquals(time, "five o' clock");
    }

    @Test
    public void test2() throws Exception {
        String time = new TheTimeInWords().translate(5, 1);
        assertEquals(time, "one minute past five");
    }

    @Test
    public void test3() throws Exception {
        String time = new TheTimeInWords().translate(5, 10);
        assertEquals(time, "ten minutes past five");
    }

    @Test
    public void test4() throws Exception {
        String time = new TheTimeInWords().translate(5, 28);
        assertEquals(time, "twenty eight minutes past five");
    }

    @Test
    public void test5() throws Exception {
        String time = new TheTimeInWords().translate(5, 30);
        assertEquals(time, "half past five");
    }

    @Test
    public void test6() throws Exception {
        String time = new TheTimeInWords().translate(5, 40);
        assertEquals(time, "twenty minutes to six");
    }

    @Test
    public void test7() throws Exception {
        String time = new TheTimeInWords().translate(5, 45);
        assertEquals(time, "quarter to six");
    }

    @Test
    public void test8() throws Exception {
        String time = new TheTimeInWords().translate(5, 47);
        assertEquals(time, "thirteen minutes to six");
    }
    
    @Test
    public void test9() throws Exception {
        String time = new TheTimeInWords().translate(10, 57);
        assertEquals(time, "three minutes to eleven");
    }
}
