package hackerrank;

import static org.junit.Assert.*;

import org.junit.Test;


public class StockMaxTest {

    @Test
    public void test1() throws Exception {
        long maxGain = new StockMax(new int[]{5, 3, 2}).calculateMaxGain();
        assertEquals(0, maxGain);
    }

    @Test
    public void test2() throws Exception {
        long maxGain = new StockMax(new int[]{1, 2, 100}).calculateMaxGain();
        assertEquals(197, maxGain);
    }

    @Test
    public void test3() throws Exception {
        long maxGain = new StockMax(new int[]{1, 3, 1, 2}).calculateMaxGain();
        assertEquals(3, maxGain);
    }

}
