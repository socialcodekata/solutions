package hackerrank;

import static org.junit.Assert.*;

import org.junit.Test;


public class BiggerIsGreaterTest {

    @Test
    public void test1() throws Exception {
        String big = new BiggerIsGreater("ab").calculateBIG();
        assertEquals("ba", big);
    }

    @Test
    public void test2() throws Exception {
        String big = new BiggerIsGreater("hefg").calculateBIG();
        assertEquals("hegf", big);
    }

    @Test
    public void test3() throws Exception {
        String big = new BiggerIsGreater("dhck").calculateBIG();
        assertEquals("dhkc", big);
    }

    @Test
    public void test4() throws Exception {
        String big = new BiggerIsGreater("dkhc").calculateBIG();
        assertEquals("hcdk", big);
    }

    @Test
    public void test5() {
        String big = new BiggerIsGreater("bb").calculateBIG();
        assertEquals("no answer", big);
    }

    @Test
    public void test6() {
        String big = new BiggerIsGreater("c").calculateBIG();
        assertEquals("no answer", big);
    }

    @Test
    public void test7() {
        assertEquals("no answer", 
                new BiggerIsGreater("zxvuutttrrrpoookiihhgggfdca").calculateBIG());
    }
}
